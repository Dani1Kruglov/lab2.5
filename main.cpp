#include <iostream>
#include <fstream>
#include <string>


struct Elem{
    int data;
    Elem* left;
    Elem* right;
    Elem* parent;
};



//создание элемента
Elem* MAKE(int data,Elem* p){
    Elem* q = new Elem;
    q -> data= data;
    q -> left= nullptr;
    q -> right= nullptr;
    q -> parent= p;
    return q;
}


//вставка элемента в дерево
void ADD(int data, Elem*& root){
    if(root == nullptr){
        root = MAKE(data,nullptr);
        return;
    }
    Elem* v = root;
    while((data < v->data && v -> left != nullptr) || (data > v->data && v -> right != nullptr)){
        if(data < v->data)
            v = v -> left;
        else
            v = v -> right;
    }
    if(data == v -> data)
        return;
    Elem* u = MAKE(data,v);
    if(data < v -> data)
        v -> left = u;
    else if(data > v->data)
        v -> right = u;
}

void PASS(Elem* v){
    if(v == nullptr)
        return;
    PASS(v -> left);
    std::cout<<v -> data<<" ";
    PASS(v -> right);
}


//поиск в дереве элемента
Elem* SEARCH(int data, Elem* v){
    if( v == nullptr)
        return v;
    if(v -> data == data)
        return v;
    if( data < v -> data)
        return SEARCH(data, v -> left);
    else  if( data > v -> data)
        return SEARCH(data, v -> right);
}

//удаление определенного элемента в дереве
void DELETE(int data, Elem*& root){
    Elem* u = SEARCH(data,root);
    if(u == nullptr)
        return;
    if( u -> left == nullptr && u -> right == nullptr && u == root){
        delete root;
        root = nullptr;
        return;
    }
    if (u->left == nullptr && u->right != nullptr && u == root){
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }
    if (u->left != nullptr && u->right == nullptr && u == root){
        Elem* t = u->left;
        while (t->right != nullptr)
            t = t->right;
        u->data = t->data;
        u = t;
    }
    if( u -> left != nullptr && u -> right != nullptr){
        Elem*t = u -> right;
        while( t -> left != nullptr)
            t = t -> left;
        u -> data = t -> data;
        u = t;
    }
    Elem* t;
    if (u->left == nullptr)
        t = u->right;
    else
        t = u->left;
    if (u->parent->left == u)
        u->parent->left = t;
    else
        u->parent->right = t;
    if (t != nullptr)
        t->parent = u->parent;
    delete u;
}

//определение глубины элемента в дереве
int DEPTH(int data, Elem* v, int k){
    if( v == nullptr){
        return -15;
    }
    if( v -> data == data){
        return k;
    }
    if( data < v -> data)
        return DEPTH(data,v->left, k+1);
    if( data > v -> data)
        return DEPTH(data,v->right, k+1);
}


void CLEAR(Elem*& v){
    if(v == nullptr)
        return;
    CLEAR(v -> left);
    CLEAR(v -> right);
    delete v;
    v = nullptr;
}

int main() {

    setlocale(LC_ALL, "RUS");

    Elem *root = nullptr;//начальное значение корня


    std::ifstream in("input.txt");
    std::ofstream out("output.txt");

    std::string s;
    int digit = 0;
    int Step = 1;

    while(getline(in, s)){

        if(s[1]=='9')
            digit = 9;
        else if(s[1]=='8')
            digit = 8;
        else if(s[1]=='7')
            digit = 7;
        else if(s[1]=='6')
            digit = 6;
        else if(s[1]=='5')
            digit = 5;
        else if(s[1]=='4')
            digit = 4;
        else if(s[1]=='3')
            digit = 3;
        else if(s[1]=='2')
            digit = 2;
        else if(s[1]=='1')
            digit = 1;
        else if(s[1]=='0')
            digit = 0;

        if(s[0] == '+')
            ADD(digit, root);
        else if(s[0] == '-')
            DELETE(digit,root);
        else if(s[0] == '?')
        {
            DEPTH(digit, root,Step);//поиск элемента в дереве
            if(DEPTH(digit, root,Step) == -15)
                out<<'n';
            else
                out<<DEPTH(digit, root,Step);
        }
        else if(s[0] == 'E')
            break;
    }
    in.close();
    out.close();
    CLEAR(root);
    return 0;
}